import React, { useEffect, useState } from 'react';
import axios from 'axios/index';
import GameGrid from './GameGrid';

const Home = () => {
	const [player, setPlayer] = useState('User');
	const [attempt, setAttempt] = useState(0);
	const [gameMode, setGameMode] = useState('');
	const [gameState, setGameState] = useState(false);
	const [game, setGame] = useState({ loading: true, data: {} });
	const [winners, setWinners] = useState({ loading: true, data: [] });

	useEffect(() => {
		async function fetchWinners() {
			const { data } = await axios(`https://starnavi-frontend-test-task.herokuapp.com/winners`);
			setWinners({ loading: false, data });
		}

		fetchWinners();

		const interval = setInterval(() => fetchWinners(), 10000);

		return () => {
			clearInterval(interval)
		}
	}, []);

	useEffect(() => {
		async function fetchData() {
			const { data } = await axios(`https://starnavi-frontend-test-task.herokuapp.com/game-settings`);
			setGame({ loading: false, data });
		}

		fetchData();
	}, []);

	useEffect(() => {
		setGameMode(Object.keys(game.data)[0]);
	}, [game.data]);

	async function setWinner(name) {
		setWinners({ loading: true, data: winners.data });
		const now = new Date();
		const options = {year: 'numeric', month: 'long', day: 'numeric', hour: '2-digit', minute: '2-digit' };

		const { data } = await axios.post(`https://starnavi-frontend-test-task.herokuapp.com/winners`, {
			winner: name,
			date: now.toLocaleDateString("en-US", options)
		});
		setWinners({ loading: false, data });
	}

	const handleSelect = (e) => {
		setGameMode(e.target.value);
	};

	const setGameSettings = (e) => {
		e.preventDefault();
		setGameState(true);
		setAttempt(attempt + 1);
	};

	const showResult = () => {
		setGameState(false);
	};


	return (
		<>
			{
				game.loading && <div className='loader'>loading game...</div>
			}
			<div className='container'>
				<div className='col'>
					<form onSubmit={ setGameSettings } className='gameForm'>
						<select className='gameForm__select' name='gameMode' onChange={ handleSelect } value={ gameMode } disabled={ gameState } placeholder={'Pick game mode'}>
							{ Object.entries(game.data).map(([key]) =>
								<option value={ key } key={ key }>{ key }</option>
							) }
						</select>
						<input className='gameForm__input' type='text' onChange={ e => setPlayer(e.target.value) } disabled={ gameState }
						       value={ player }/>
						<input className='gameForm__submit' type='submit' value={ attempt > 0 && !gameState ? 'play again' : 'play' }/>
					</form>
					<GameGrid
						gameMode={ game.data[gameMode] }
						gameState={ gameState }
						stop={ showResult }
						player={ player }
						attempt={ attempt }
						setWinner={ setWinner }
					/>
				</div>
				<div className='col'>
					<h2>Leader Board</h2>
					<table className='leaders'>
						<tbody>
						{ winners.data.map((entry, index) =>
							<tr key={ 'winner_' + index }>
								<td>{ entry.winner }</td>
								<td>{ entry.date }</td>
							</tr>
						) }
						</tbody>
					</table>
				</div>
			</div>
		</>
	);
};

export default Home;
