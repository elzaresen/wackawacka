import React, { useEffect, useState } from 'react';
import * as cellStatus from './../../constants/cellStatus';

const GameGrid = (props) => {
	const [winner, renderWinner] = useState('');
	const [cells, setCells] = useState({});
	const [highlight, setHighlight] = useState(null);
	const { gameState, gameMode, stop, attempt, setWinner } = props;

	useEffect(() => {
		const cells = {};
		for (let i = 0; i < props.gameMode?.field ** 2; i++) {
			cells[i] = cellStatus.INIT;
		}
		setCells({ ...cells });
		renderWinner('');
	}, [gameMode, attempt]);

	useEffect(() => {
		const keys = Object.keys(cells).filter(cell => cells[cell] === cellStatus.INIT);

		const interval = setInterval(() => {
			const randomCell = +keys[Math.floor(Math.random() * (keys.length))];
			setHighlight(randomCell);
			cells[highlight] !== cellStatus.USER && setCellResult(highlight, cellStatus.PC);
		}, gameMode?.delay);

		!gameState && clearInterval(interval);

		return () => {
			clearInterval(interval);
		};
	}, [cells, highlight, gameState]);

	useEffect(() => {
		const userScore = Object.keys(cells).filter(cell => cells[cell] === cellStatus.USER).length;
		const pcScore = Object.keys(cells).filter(cell => cells[cell] === cellStatus.PC).length;
		const winningScore = (props.gameMode?.field ** 2) / 2;

		if (userScore > winningScore) {
			renderWinner(props.player + ' wins');
			setWinner(props.player);
		}
		if (pcScore > winningScore) {
			renderWinner('Computer wins');
			setWinner('Computer');
		}

		if (userScore > winningScore || pcScore > winningScore) {
			stop();
			setHighlight(null);
		}
	}, [cells]);

	const containerStyle = {
		gridTemplateColumns: Array.from(new Array(props.gameMode?.field), () => {
			return `${ (500 / props.gameMode?.field) - 2 }px`;
		}).join(' '),
		gridTemplateRows: Array.from(new Array(props.gameMode?.field), () => {
			return `${ (500 / props.gameMode?.field) - 2 }px`;
		}).join(' '),
	};

	const setCellResult = (cellId, status) => {
		const _cells = cells;
		if (+cellId === highlight) {
			_cells[cellId] = status;
			setCells({ ..._cells });
		}
	};

	const handleClick = (e) => {
		+e.target.id === highlight && setCellResult(e.target.id, cellStatus.USER);
	};

	const renderCells = () => {
		const elements = [];
		for (let cell in cells) {
			elements.push(
				<button
					id={ cell }
					key={ 'btn-' + cell }
					className={ `
					cell 
					${ highlight === +cell ? 'cell--highlight' : '' } 
					${ +cells[cell] === cellStatus.USER ? 'cell--ok' : '' }
					${ +cells[cell] === cellStatus.PC ? 'cell--fail' : '' }
					` }
					disabled={ +cells[cell] === cellStatus.USER }
					onClick={ handleClick }
				/>
			);
		}
		return elements;
	};

	return (
		<>
			<p>{ winner || <span>&nbsp;</span> }</p>
			<div className='playground'>
				<div className='playground__container' style={ containerStyle }>
					{ renderCells() }
				</div>
			</div>
		</>
	);
};

export default GameGrid;